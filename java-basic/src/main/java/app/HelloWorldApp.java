package app;

/**
 * browser:  https://github.com/alexr007/fs6-online
 * git:      git@github.com:alexr007/fs6-online.git
 * git clone git@github.com:alexr007/fs6-online.git
 * git clone https://github.com/alexr007/fs6-online.git
 *
 */
public class HelloWorldApp {
  //                 main:String[] => void
  public static void main(String[] args) {
    String message = "Hello World";
    System.out.println(message);
    if (args.length >= 2) {
      String p1 = args[0];
      String p2 = args[1];
      System.out.println(p1); // Jim
      System.out.println(p2); // 33
    }
  }

}
