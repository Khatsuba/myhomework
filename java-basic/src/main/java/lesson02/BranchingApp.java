package lesson02;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.Scanner;

public class BranchingApp {

  public static void main(String[] args) {
    InputStream in = System.in;
    PrintStream out = System.out;
    Scanner sc = new Scanner(in);
    out.println("Hello, Enter the number");
    int x = sc.nextInt();
    if (x % 2 == 0) {
      out.printf("%d can be divided by 2", x);
    } else {
      out.printf("%d can't be divided by 2", x);
    }
  }

}
