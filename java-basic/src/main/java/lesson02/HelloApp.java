package lesson02;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.Scanner;

public class HelloApp {

  public static void main(String[] args) {
    InputStream in = System.in;
    PrintStream out = System.out;
    Scanner sc = new Scanner(in);
    out.println("Hello, What is your name?");
    String name;
    name = sc.next();
    String message = String.format("Hello, %s!", name);
    out.println(message);
  }

}
