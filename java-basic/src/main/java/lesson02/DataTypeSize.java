package lesson02;

public class DataTypeSize {

  public static void main(String[] args) {
    float a = 1 / 3;
    System.out.println(a);
    float b = (float)1 / 3;
    System.out.println(b);
    double c = (double) 1 / 3;
    System.out.println(c);
  }

}
