package lesson03;

public class IterationsApp {

  public static void main1(String[] args) {
    int count = 30;

    while (count > 0) {
      System.out.print("*");
      count--;
    }

  }

  public static void main2(String[] args) {
    final int count = 30;

    int i = 0;
    while (i < count) {
      System.out.print("*");
      i++; // i += 1; // i = i + 1;
    }

  }

  public static void main(String[] args) {
    final int count = 30;

    for (int i = 0; i < count; i++) {
      System.out.print("*");
    }

  }

  public static void main9(String[] args) {
    final int count = 30;

    for (int i = 0; i < count; i++, System.out.print("*"));

  }

  public static void main8(String[] args) {
    final int count = 30;

    int i = 0;
    for (; i < count; i++) {
      System.out.print("*");
    }
    System.out.println(i);
  }

  public static void main5(String[] args) {
    final int count = 30;

    for (int i = 0; ; i++) {
      if (i < count) System.out.print("*");
      else break;
    }

  }

  public static void main6(String[] args) {
    final int count = 30;

    for (int i = 0; ; ) {
      if (!(i < count)) break;
      System.out.print("*");
      i++;
    }

  }

  public static void main7(String[] args) {
    int count = 30;

    for ( ; ; ) {
      if (count == 0) break;
      System.out.print("*");
      count--;
    }

  }

}
