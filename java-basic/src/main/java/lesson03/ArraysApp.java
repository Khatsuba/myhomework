package lesson03;

public class ArraysApp {

  static String convert2(int[] xs) {
    String s = "[";
    for (int i = 0; i < xs.length; i++) {
      s = s + (i > 0 ? ", ": "");
      s = s + xs[i];
    }
    return s + "]";
  }

  static String convert(int[] xs) {
    StringBuilder sb = new StringBuilder("[");
    for (int i = 0; i < xs.length; i++) {
      if (i > 0) sb.append(", ");
      sb.append(xs[i]);
    }
    sb.append(']');
    return sb.toString();
  }

  public static void main(String[] args) {
    int[] xs2 = {10, 20, 30, 40};
    System.out.println(convert(xs2));
  }

  public static void main1(String[] args) {

    int[] xs = new int[10];
    int[] xs2 = {10, 20, 30, 40};

    int first =  xs2[0]; // 10
    int second = xs2[1]; // 20
    int third =  xs2[2]; // 30
    int fourth = xs2[3]; // 40

    int l1 = xs2.length; // 4
    int l2 = xs.length; // 10

    // there are no negative indexes in JAVA
    // System.out.println(xs2[-1]);

    for (int i = 0; i < xs2.length; i++) {
      System.out.printf("%d ", xs2[i]);
    }

    System.out.println();

    // Java 5+
    for (int x : xs2) {
      System.out.printf("%d ", x);
    }

  }

}
