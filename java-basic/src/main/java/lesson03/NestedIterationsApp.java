package lesson03;

import java.io.PrintStream;

public class NestedIterationsApp {

  /**
   * *----------------------
   * |  \    /
   * |   \ /
   * |   /\
   * | /   \
   * *------\---------------
   *
   * \\\\\\\
   * ///////
   *
   */
  public static void main1(String[] args) {
    PrintStream out = System.out;
    for (int row = 1; row < 7; row ++) {
      for (int col = 0; col < 30; col++) {
//        if     (..)
          out.print("*");
//        else if(..) out.print("\\");
//        else if(..) out.print("/");
//        else if(..) out.print("-");
//        else if(..) out.print("|");
//        else        out.print(" ");
      }
      System.out.println();
    }
  }

  public static void main2(String[] args) {
    int i = 10;
    while (i > 0) {
      System.out.print("*");
      i --;
    }
  }

  public static void main(String[] args) {
    // min 1 pass
    int i = 10;
    do {
      System.out.print("*");
      i--;
    } while (i > 0);
  }

}
