package lesson04;

import java.io.InputStream;
import java.util.Scanner;

public class ScannerApp {

    static boolean isInt(String raw) {
        try {
            int n = Integer.parseInt(raw);
            return true;
        } catch (NumberFormatException x) {
            return false;
        }
    }

    static int toInt(String raw) {
        return Integer.parseInt(raw);
    }

    static boolean validate(int x) {
        return x > 0 && x < 10;
    }

    public static void main(String[] args) {
        InputStream in = System.in;
        Scanner sc = new Scanner(in);

        int x;

        while (true) {
            System.out.print("Enter the number:");
            String s = sc.nextLine();
            if (isInt(s)) {
                x = toInt(s);
                if (validate(x)) break;
                else System.out.println("Bad number given");
            } else System.out.println("Not number given.");
        }

        System.out.printf("OK: %d", x);

    }
}
