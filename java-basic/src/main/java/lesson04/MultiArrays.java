package lesson04;

public class MultiArrays {

    public static void main(String[] args) {
        int[][][][] xssss =
                {
                  // ------------------------------------------ xsss
                  // ----------------                           xss
                  //   -------                                  xs
                   { { {1,2,3}, {4} }, {{5,6}, {}}, {{}, {}}, }, //
                   { {{}, {}}, {{}, {}},},                       //
                   { {{}, {}},},                                 //
                   {          },                                 //
                };
        int[][][] xsss = xssss[0];
        int[][] xss = xsss[0];
        int[] xs = xss[0];
        int x = xs[0];

        int [][][] x3 = xssss[3];
        System.out.println(x3.length); // 0
        System.out.println(xs[3]);     // ArrayIndexOutOfBoundsException: Index 3 out of bounds for length 3
    }

}
