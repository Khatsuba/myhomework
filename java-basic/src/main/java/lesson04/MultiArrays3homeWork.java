package lesson04;

public class MultiArrays3homeWork {

    // TODO
    static int calculateElement(int x, int y) {
        throw new RuntimeException("TODO");
    }

    static void fill(int[][] data) {
        int W = data[0].length;
        int H = data.length;

        for (int y = 0; y < H; y++) {
            for (int x = 0; x < W; x++) {
                data[y][x] = calculateElement(x, y);
            }
        }
    }

    // TODO
    static int calculateX(int i) {
        throw new RuntimeException("TODO");
    }

    // TODO
    static int calculateY(int i) {
        throw new RuntimeException("TODO");
    }

    // TODO
    static boolean isNewline(int i) {
        throw new RuntimeException("TODO");
    }

    public static void print(int[][] data) {
        int W = data[0].length;
        int H = data.length;
        for (int i = 0; i < W * H ; i++) {
            int x = calculateX(i);
            int y = calculateY(i);
            int el = data[y][x];
            System.out.printf("%3d ", el);
            if (isNewline(i)) System.out.println();
        }
    }

    public static void main(String[] args) {
        int[][] xss = new int[10][20];

        fill(xss);

        print(xss);
    }

}
