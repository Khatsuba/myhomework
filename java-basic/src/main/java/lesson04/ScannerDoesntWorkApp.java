package lesson04;

import java.io.InputStream;
import java.util.Scanner;

public class ScannerDoesntWorkApp {

    static boolean validate(int x) {
        return x > 0 && x < 10;
    }

    public static void main(String[] args) {
        InputStream in = System.in;

        Scanner sc = new Scanner(in);

        int x = 0;

        System.out.print("Enter the number:");
        while (!sc.hasNextInt()) {
            System.out.println("Not number given");
            String s = sc.next();
            x = Integer.parseInt(s);
//            if (sc.hasNextInt()) {
//                x = sc.nextInt();
//                if (validate(x)) break;
//                else System.out.println("Wrong number given");
//            } else System.out.println("Not number given");
        }

        System.out.printf("OK: %d", x);

    }
}
