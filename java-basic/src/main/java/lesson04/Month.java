package lesson04;

public class Month {

    /**
     * 1  -> 31 | "31"
     * 2  -> 28 | "30"
     * 3  -> 31 | "31"
     * 4  -> 30 | "30"
     * 5  -> 31 | "31"
     * 6  -> 30 | "30"
     * 7  -> 31 | "31"
     * 8  -> 31 | "30"
     * 9  -> 30 | "31"
     * 10 -> 31 | "30"
     * 11 -> 30 | "31"
     * 12 -> 31 | "30"
     * --------
     * NO:
     * - arrays
     * - for
     * - while
     * - if
     * - switch
     * - tern op
     * ------------
     * +
     * -
     * *
     * /
     * %
     * ------------
     * month: 1..12
     */
    public static int nDays(int month) {
        return 30 + month % 2;
    }

    public static void main(String[] args) {
        System.out.println(nDays(1));  // 31
        System.out.println(nDays(2));  // 28
        System.out.println(nDays(3));  // 31
        //..
        System.out.println(nDays(12)); // 31
    }

}
