package lesson04;

public class MultiArrays2 {

    static void fill(int[][] data) {
        int W = data[0].length;
        int H = data.length;

        int i = 1;

        for (int y = 0; y < H; y++) {
            for (int x = 0; x < W; x++) {
                data[y][x] = i;
                i++;
            }
        }
    }


    public static void main(String[] args) {
        int[][] xss = new int[10][20];
        fill(xss);

        int W = xss[0].length;
        int H = xss.length;

        for (int y = 0; y < H; y++) {
            for (int x = 0; x < W; x++) {
                System.out.printf("%3d ", xss[y][x]);
            }
            System.out.println();
        }

        System.out.println();

        for (int x = 0; x < W; x++) {
           for (int y = 0; y < H; y++) {
                System.out.printf("%3d ", xss[y][x]);
            }
            System.out.println();
        }

    }

}
