package lesson04;

public class ClassesProblem {

    static void doWhateverYouWant1(
            String personName,
            int yearOfBirth,
            int monthOfBirth,
            int dayOfBirth) {
        // TODO
    }

    static void doWhateverYouWant2(
            Person2 p) {
        // TODO
    }

    public static void main(String[] args) {

        // problem
        String personName = "Alex";
        int yearOfBirth = 2000;
        int monthOfBirth = 11;
        int dayOfBirth = 11;

        /** step 1*/
        // solution
        //     ======= object, instance
        //     V
        Person1 p =
                //     ====== class
                //     V
                new Person1();
        p.personName = "Jim";
        p.yearOfBirth = 1975;
        p.monthOfYear = 8;
        p.dayOfMonth = 13;

        /** step2 */
        Date dt = new Date();
        dt.year = 1975;
        dt.monthOfYear = 8;
        dt.dayOfMonth = 13;

        Person2 p2 = new Person2();
        p2.name = "Jackson";
        p2.birth = dt;


    }

}
