package lesson05;

public class Sea1 {

    /**
     * 0 - empty .
     * 1 - boat  *
     * 2 - try   x
     * 3 - hit   X
     */
    public static String represent(int value) {
        return switch (value) {
            case 1 -> "*";
            case 2 -> "x";
            case 3 -> "X";
            default -> " ";
        };
    }

    public static void arrangeShips(int[][] board) {
        board[0][0] = 1;
        board[0][1] = 1;
        board[0][2] = 1;
        board[0][3] = 3;
        board[5][5] = 2;
    }

    public static String representBoard(int[][] board) {
        StringBuilder sb = new StringBuilder();
        for (int y = 0; y < board.length; y++) {
            sb.append("| ");
            for (int x = 0; x < board[y].length; x++) {
                if (x > 0) sb.append(" | ");
                sb.append(represent(board[y][x]));
            }
            sb.append(" |\n");
        }
        return sb.toString();
    }

    public static void drawBoard(int[][] board) {
        System.out.println(representBoard(board));
    }

    public static void main(String[] args) {
        final int SIZE = 10;
        int[][] board = new int[SIZE][SIZE];
        arrangeShips(board);
        drawBoard(board);
    }

}
