package lesson05;

public class Sea3 {

    final int SIZE = 10;
    int[][] board = new int[SIZE][SIZE];

    /**
     * 0 - empty .
     * 1 - boat  *
     * 2 - try   x
     * 3 - hit   X
     */
    public String represent(int value) {
        return switch (value) {
            case 1 -> "*";
            case 2 -> "x";
            case 3 -> "X";
            default -> " ";
        };
    }

    /**
     * random +
     *
     * size count
     * ---- -----
     * [
     *  [4,  1],
     *  [3,  2],
     *  [2,  3],
     *  [1,  4]
     * ]
     *
     * [[1, 8]]
     *
     */
    public void arrangeShipsSmart(int[][] ships) {

    }

    public void arrangeShips() {
        board[0][0] = 1;
        board[0][1] = 1;
        board[0][2] = 1;
        board[0][3] = 3;
        board[5][5] = 2;
    }

    public String representBoard() {
        StringBuilder sb = new StringBuilder();
        for (int y = 0; y < board.length; y++) {
            sb.append("| ");
            for (int x = 0; x < board[y].length; x++) {
                if (x > 0) sb.append(" | ");
                sb.append(represent(board[y][x]));
            }
            sb.append(" |\n");
        }
        return sb.toString();
    }

    public void drawBoard() {
        System.out.println(representBoard());
    }

    public static void main(String[] args) {
        Sea3 game = new Sea3();
        game.arrangeShips();
        game.drawBoard();
    }

}
