package lesson05;

import java.util.Arrays;
import java.util.Random;

public class RandomApp {

    public static void main(String[] args) {

        // year of birth: Int
        // 1950..2010
        // 1..7
        // 1..30
        // 1..12
        // 18..70
        // 10..20

        // [0..1)
        //  0..0.999999
        double rx = Math.random();
        // [0..10]

        final int SIZE = 20;
        int[] xs = new int[SIZE];
        for (int i = 0; i < xs.length; i++) {
            xs[i] = (int)(Math.random() * (70 - 18 + 1) + 18);
        }
        System.out.println(xs);
        System.out.println(Arrays.toString(xs));

    }

}
