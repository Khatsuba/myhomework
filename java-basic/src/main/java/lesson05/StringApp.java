package lesson05;

public class StringApp {

    public static void main(String[] args) {

        String s = "Hello";
        String w = new String("Планета");

        s.toUpperCase(); // "HELLO"
        s.toLowerCase(); // "hello"

        s.concat(", ").concat(w); // "Hello, World"

        s.startsWith("He"); // true
        w.endsWith("ld"); // true

        String[] ss = "qwe gfdh ghf hgj".split(" "); // ["qwe", "gfdh", "ghf", "hgj"]

        System.out.println(s.length()); // 5
        System.out.println(w.length()); // 7
        // https://www.fileformat.info/info/charset/UTF-8/list.htm?start=1024
        System.out.println(s.getBytes().length); // 5
        System.out.println(w.getBytes().length); // 14
        char c = s.charAt(3);
        "hello".compareToIgnoreCase("Hello"); // true
        s.indexOf("l"); // 2
        "".isEmpty(); // true
        " \n\t".isBlank(); // true
        char[] charArray = s.toCharArray();
        byte[] byteArray = s.getBytes();
        String s1 = new String(charArray);
        String s2 = new String(byteArray);

    }
}
