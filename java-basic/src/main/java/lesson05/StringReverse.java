package lesson05;

public class StringReverse {

    public static String reverse1(String in) {
        String out = "";
        for (int i = 0; i < in.length(); i++) {
            out = String.format("%s%s", in.charAt(i), out);
        }
        return out;
    }

    public static String reverse2(String in) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < in.length(); i++) {
            sb.append(in.charAt(in.length() - i - 1));
        }
        return sb.toString();
    }

    public static String reverse3(String in) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < in.length(); sb.append(in.charAt(in.length() - 1 - i++))) ;
        return sb.toString();
    }

    public static String reverse4(String in) {
        int l = in.length();
        char[] chars = new char[l];
        for (int i = 0; i < l; i++) {
            chars[i] = in.charAt(l - 1 - i);
        }
        return new String(chars);
    }

    public static String reverse5(String in) {
        byte[] bytes = in.getBytes();
        int l = in.length();
        for (int i = 0; i < l / 2; i++) {
            byte x = bytes[i];
            bytes[i] = bytes[l - 1 - i];
            bytes[l - 1 - i] = x;
        }
        return new String(bytes);
    }

    public static String reverse6(String in) {
        return new StringBuilder(in).reverse().toString();
    }

    public static void main(String[] args) {
        String s1 = "Hello"; // olleH
        System.out.println(reverse1(s1));
        System.out.println(reverse2(s1));
        System.out.println(reverse3(s1));
        System.out.println(reverse4(s1));
        System.out.println(reverse5(s1));
        System.out.println(reverse6(s1));
    }
}
