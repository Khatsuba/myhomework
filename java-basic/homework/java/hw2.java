import java.util.Scanner;

public class hw2 {
    public static void fillArray(char[][] in) {
        for (int i = 0; i< in.length; i++){
            for (int j = 0; j< in[0].length; j++){
                in[i][j]='-';
            }
        }
    }

    public static void outputArray(char[][] in) {
        System.out.println("0 | 1 | 2 | 3 | 4 | 5 |");
        int l = 1;
        for (int i = 0; i< in.length; i++){
            System.out.printf("%s | ", l);
            for (int j = 0; j< in[0].length; j++){
                System.out.printf("%c | ", in[i][j]);
            }
            System.out.println();
            l++;
        }
    }

    public static void main(String[] arg) {
        char[][] map = new char[5][5];
        Scanner sc = new Scanner(System.in);
        int x, y;
        int targy = (int) (Math.random()*5+1);
        int targx = (int) (Math.random()*5+1);
        fillArray(map);
        //map[targx-1][targy-1] = 'o';

        System.out.println("All Set. Get ready to rumble!");
        outputArray(map);
        while(true) {
            System.out.print("pick row:");
            while (!sc.hasNextInt()) {
                System.out.print("not a number \npick column:");
                sc.next();
            }
            x = sc.nextInt();
            System.out.print("pick column:");
            while (!sc.hasNextInt()) {
                System.out.print("not a number \npick row:");
                sc.next();
            }
            y = sc.nextInt();

            if (x < 6 && x > 0 && y < 6 && y > 0) {
                if (targx == x && targy == y) {
                    System.out.println("You won!!!");
                    map[x-1][y-1] = 'X';
                    outputArray(map);
                    break;
                }
                else {
                    map[x-1][y-1] = '*';
                    System.out.println("You missed!");
                }
            }
            else System.out.println("Oops, that's definitely a miss");
            outputArray(map);
        }
    }
}
