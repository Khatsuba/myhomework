import java.util.Scanner;

public class hw3 {
    public static void fillArray(String[][] in) {
        in[0][0] = "Sunday";
        in[0][1] = "do home work";
        in[1][0] = "Monday";
        in[1][1] = "watch a film";
        in[2][0] = "Tuesday";
        in[2][1] = "take a yoga class";
        in[3][0] = "Wednesday";
        in[3][1] = "hike in the nearby park";
        in[4][0] = "Thursday";
        in[4][1] = "have a picnic with friends";
        in[5][0] = "Friday";
        in[5][1] = "read a new book";
        in[6][0] = "Saturday";
        in[6][1] = "visit the art museum";
    }
    public static void main(String[] args) {
        String[][] schedule = new String[7][2];
        fillArray(schedule);
        while(true){
            System.out.println("Please, input the day of the week: ");
            Scanner sc = new Scanner(System.in);
            String day = sc.nextLine();
            day = day.toLowerCase().trim();
            if (day.contains("change"))
            {
                day = day.substring(7).trim();
                System.out.printf("Please, input new tasks for %s. \n", day);
                String tasks = sc.nextLine();
                switch(day) {
                    case "sunday":      schedule[0][1] = tasks;break;
                    case "monday":      schedule[1][1] = tasks;break;
                    case "tuesday":     schedule[2][1] = tasks;break;
                    case "wednesday":   schedule[3][1] = tasks;break;
                    case "thursday":    schedule[4][1] = tasks;break;
                    case "friday":      schedule[5][1] = tasks;break;
                    case "saturday":    schedule[6][1] = tasks;break;
                    default: System.out.println("Sorry, I don't understand you, please try again.");
                }
            }
            else{
                switch(day){
                    case "sunday":      System.out.println(schedule[0][1]);break;
                    case "monday":      System.out.println(schedule[1][1]);break;
                    case "tuesday":     System.out.println(schedule[2][1]);break;
                    case "wednesday":   System.out.println(schedule[3][1]);break;
                    case "thursday":    System.out.println(schedule[4][1]);break;
                    case "friday":      System.out.println(schedule[5][1]);break;
                    case "saturday":    System.out.println(schedule[6][1]);break;
                    case "exit": return;
                    default: System.out.println("Sorry, I don't understand you, please try again.");
                }
            }
        }
    }
}
