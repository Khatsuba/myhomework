import java.util.Scanner;

public class hw1 {
    public static String[] sortArray(String arg)
    {
        String[] Sorted = arg.split(" ");
        for (int i = 0; i < Sorted.length - 1; i++) {
            for (int j = 0; j < Sorted.length - i - 1; j++) {
                if (Integer.parseInt(Sorted[j + 1]) < Integer.parseInt(Sorted[j])) {
                    String swap = Sorted[j];
                    Sorted[j] = Sorted[j + 1];
                    Sorted[j + 1] = swap;
                }
            }
        }
        return Sorted;
    }

    public static int generateNumber() {
        String[][] dates =
                {
                        {"1945", "When did World War II officially end?"},
                        {"1822", "When was the first computer built?"},
                        {"1989", "When did the Berlin Wall fall?"},
                        {"1969", "When did NASA's Apollo 11 mission land on the Moon?"},
                        {"1903", "When was the first successful powered flight by the Wright brothers?"},
                        {"1492", "When did Christopher Columbus first reach the Americas?"},
                        {"1692", "When did the Salem witch trials take place?"},
                        {"1804", "When did Napoleon Bonaparte become Emperor of the French?"},
                        {"1096", "When did the First Crusade begin?"},
                        {"1778", "When did Captain James Cook explore the Hawaiian Islands?"},
                };
        int x = (int) (Math.random() * 10);
        System.out.println(dates[x][1]);
        return Integer.parseInt(dates[x][0]);
    }



    public static void main(String[] args)
    {
        int guess = 0;
        String guesses = "";
        Scanner sc = new Scanner(System.in);
        System.out.print("What's your name? ");
        String name = sc.next();
        System.out.println("Let the game begin!");
        int number = generateNumber();
        while (true)
        {
            System.out.print("your guess:");
            while (!sc.hasNextInt())
            {
                System.out.print("not a number \nyour guess:");
                sc.next();
            }
            guess = sc.nextInt();
            guesses = guesses + guess + " ";

            if (guess<number) { System.out.println("Your number is too small. Please, try again.");}
            else if (guess>number) { System.out.println("Your number is too big. Please, try again.");}
            else { break;}
        }
        String[] Allguesses = sortArray(guesses);
        System.out.printf("Congratulations, %s, you won!\nHere are all your (%s) guesses: ", name, Allguesses.length);
        for (int i = 0; i < Allguesses.length; i++) {
        System.out.print(Allguesses[i] + " "); }
    }
}
